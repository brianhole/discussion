*This is a new project - Any feedback is greatly appreciated 
(by [opening a discussion thread (issue)](https://gitlab.com/publishing-reform/discussion/issues/new) or sending an email)*

# Public Discussion on Publishing Reform

#### [The list of all discussion topics](https://gitlab.com/publishing-reform/discussion/issues)

## Our vision
Our goal is to simultaneously open scholarly publishing to the society and 
raise the standards by making it fairer, more efficient and productive for the researchers.
We would like to maximize everyone's efforts and save everyone's time, 
including members of this project.

## How can this board save your time?

### Selected participation
You can select discussions threads where you want to participate and get email notifications.
Unlike email lists, where you would receive all the correspondence, 
here you can selectively subscribe to or unsubscribe from any thread.

### Notifications
You are automatically subscribed to any new thread you start or add comments to.
That means, you receive any new comments by email.
However, you can unsubscribe from any thread at any time
by simply clicking the "unsubscribe" link in your notification email.

To inspect or change the subscription status, go to any thread, 
e.g. [this one](https://gitlab.com/publishing-reform/discussion/issues/2),
where on the right panel at the bottom (you might need to scroll)
you see the Notification tab that you can toggle between Subscribe and Unsubscribe.

### Global notification settings
If you would like to recieve notification for evey new thread,
you can [go to the main project page](https://gitlab.com/publishing-reform/discussion)
and click on the "Global" pull-down menu with the bell icon.
The menu is on the very right tab right below the description.
There you can change "Global" to other values,
for instance to "Watch" to receive email notifications for any activity.
You can even selectively customize the settings by choosing the "Custom" option at the bottom of the menu.

See here for more information about Gitlab Notifications: https://docs.gitlab.com/ee/workflow/notifications.html#notification-settings

### Email communication

Instead of logging to the project, you can do all the communication by email.
You can start a discussion by email (click the link "Email a new issue to this project" under 
[the list of all threads](https://gitlab.com/publishing-reform/discussion/issues)
or reply to any notification email to add a comment to the discussion.

See here for more information about Email Notifications: https://docs.gitlab.com/ee/workflow/notifications.html#gitlab-notification-emails

### Explicit mention
By writing "@" in front of any member username, you can explicitly mention this member in any discussion,
as [in this example](https://gitlab.com/publishing-reform/working-group/issues/1#note_57216096).
The member will automatically receive an email notification, which is less formal than a direct email.
The receiver can then reply directly to the email or click the Gitlab link and comment within the project.

See here for more information about Discussion Mentions: https://docs.gitlab.com/ee/user/project/issues/issues_functionalities.html#13-mentions


## More User Help

### Reading discussion threads

To [see the list of all discussions](https://gitlab.com/publishing-reform/discussion/issues),
click the `Issues` tab from the left navigation bar (4th icon from the top). 
You can search, filter and sort the discussions there.

When you see any discussion of interest, you can enter it by clicking and then add your comment at the bottom.
You can also subscribe to the updates for any thread by selecting "Subscribe" in the Notification bar
at the very bottom on the right navigation panel (you may need to scroll down).

### Opening a new discussion

To [start new discussion thread](https://gitlab.com/publishing-reform/discussion/issues/new), 
click the `Issues` tab from the left navigation bar (4th icon from the top) and then the green button "New issue"
in the right top corner. Alternatively, from [the main project page](https://gitlab.com/publishing-reform/discussion),
you can click the `+` in the navigation bar right below the description, and then select "New issue".
Simply add the title and description and click on the green button "Submit issue".

### Files
All files are shown on [the main project page](https://gitlab.com/publishing-reform/discussion).
(At the time of writing there is only this README.md file.)
You can dowload the entire file collection with a single click 
on the download icon (last to the right of the "Find file" button).

To upload a new file, click on the pull-down menu `+` on [the main project page](https://gitlab.com/publishing-reform/discussion)
in the navigation bar above (3rd navigation row below the description) and select `Upload file`.

### Formatting
For advanced formatting, title/subtitle structure, itemization, images, videos, mathematics formulas and much more,
use the Markdown language https://docs.gitlab.com/ee/user/markdown.html
