Details of publishing options compatible with Fair Open Access
======

## [Scholastica]

See https://gitlab.com/publishing-reform/discussion/blob/master/Service%20Providers/Scholastica.md
for more information

## Mersenne

Mersenne (as used by Algebraic Combinatorics) is supported by CNRS and Universite Grenoble-Alpes where it is hosted. It can offer (for free, but capacity may be limited) a customized OJS editorial software with IT support, DOIs, plagiarism checking, with other services such as typesetting offered a la carte. 

More details:

* [Mersenne site] (http://www.mersenne.fr/en/about-mersenne/)
* [Algebraic Combinatorics] (http://algebraic-combinatorics.org/)


## Ubiquity Press (main contact May 2016)

Basically a full-service operation, the one behind the Open Library of Humanities. Costs range from the hundreds of dollars/euros per year (for hosting of the journal and hardware and software maintenance, with minimal additional services) to the hundreds of dollars/euros per article (for a complete process, including staff time to support the peer-review process, typesetting and copyediting, full production). Intermediate prices are possible, the services can be personalised feature by feature. The editorial system is modern, based on Open Journal Systems, but nicer.

The company is a spinoff from University College London: http://www.ubiquitypress.com/

More details:

* [Example journal publishing with Ubiquity] (http://www.glossa-journal.org) (part of https://www.openlibhums.org/)
* [An editor training video] (https://vimeo.com/144631620) (password: JMSTraining)

The three levels of service:

* Stage 1: Ubiquity Press simply hosts the journals on our platform and ensures that they are fully functional and available. The client institution is responsible for editorial support and ensuring that functions such as peer review, copyediting and typesetting are carried out. These journals have a flat annual fee of £500 or ca. €715.

* Stage 2: Essentially stage one journals that have elected to also use some stage three features (e.g. typesetting). In this case the stage one fee is charged, plus an additional fee for the individual stage three work/services.

* Stage 3: Here we provide a full stack of publishing services, including dedicated editorial support, anti-plagiarism checking, copyediting, typesetting, indexing and archiving and content promotion, etc.. These journals have no annual charge, but do incur a fee per article published of £300 or ca. €572. These fees are generally paid by either the Society, or by the author's funder or institution [Note by MathOA: compliance with Fair OA principle necessarily entails that the APC are paid journal-side, e.g. by MathOA, rather than author-side]


## T&T (main contact November 2016)
======

A company with much experience in typesetting and copyediting mathematical-heavy articles and books (they currently work for several mainstream mathematical journals). They do not publish journals from peer-review to production yet, but have experience in project management (e.g. the book Princeton Companion to Mathematics) and could extend their services, or can be used for the production part on top of another service provider.

Prices are highly customizable depending on the variety and level of service, the full service not exceeding a few hundred dollars/euros for a medium-sized paper.

Example journals typeset by T&T:
Proceedings of the Royal Society of Edinburgh (Series A) 
Journal de l’Institut Mathématique de Jussieu

PsychOpen (last updated June 2017)
======

[PsychOpen] (http://www.psychopen.eu/) is an open-access publishing platform for psychology, operated by Leibniz Institute for Psychology Information (ZPID) [https://www.zpid.de/en], Trier, Germany. Currently, seven journals are published with PsychOpen. The PsychOpen platform is part of ZPID's  overall open science strategy in psychology (free search portal PubPsych, data archive PsychData, repository PsychArchives, etc).

Publishing service is free of charge (no APCs or any other fees).  PsychOpen mainly supports journals based in Europe or with a European focus.

Supported journals receive full publishing service, including:
* hosting journal management system (OJS) and technical support;
* editorial and user support;
* publication quality control, including anti-plagiarism checking (iThenticate), automated review of basic statistical results (Statcheck), reference list checking (eXtyles), APA style review;
* copyediting (eXtyles), converting MS Word submissions into high-quality XML (JATS) format, equations are transformed to MathML (LaTeX submission is not supported);
* page layout and production (Antenna House Formatter);
* DOI registration (Crossref);
* indexing (e.g., Scopus, PubMed Central, DOAJ);
* long-term archiving (CLOCKSS);
* content promotion support.

They do not support LaTeX submission. ORCID integration is in preparation (as of 2017) 

Articles are published under a CC-BY license, authors retain ownership of the copyright.

PsychOpen adheres to mandatory quality criteria (e.g., peer review, endorsement by learned society, international editorial board, etc.) so that the journals under its roof acquire a good reputation.

Example journals published with PsychOpen:

* [Journal of Numerical Cognition] (http://jnc.psychopen.eu/)
* [Journal of Social and Political Psychology] (http://jspp.psychopen.eu/)
* [Europe's Journal of Psychology] (http://ejop.psychopen.eu/)

