Concerns about converting a journal to a Fair Open Access Model
======


What is there to worry about?
======

* The existing publisher may have the rights to the title of the journal. 
* The publisher will control access to the back issues of the journal.
* My current contract probably contains a no-compete clause.
* Readers and administrators may be confused about quality.
* The old journal's ranking/reputation will go down ("zombie" status), thus damaging the career gains for authors who published in it.
* It may take a long time for the new journal to be listed on MathSciNet, Scopus, and get an impact factor, etc.
* The converted journal may not be financially stable. I am not ready to run a volunteer, amateur journal.
* The transition process may involve a lot of work.
* What about my editorial stipend?
* I need good editorial software as provided by my current publisher.

What can be done to address these concerns?
======

* Title: Several mathematics journals have [left commercial publishers and changed their name] (http://oad.simmons.edu/oadwiki/Journal_declarations_of_independence). The right way to do this is clear. A unanimous decision by the editorial board, advertised openly and widely to the community, makes it very likely that the original journal will cease production fairly quickly because of lack of editors and submissions. Authors usually agree to withdraw their papers from the old journal and submit them to the new one. Mathematical journals that have done this switch successfully include Journal of Algorithms, Topology and Topology and its Applications. Their replacements (ACM Transactions on Algorithms, Journal of Topology, and Algebraic & Geometric Topology) are all highly regarded. 

We have recent direct experience with the case of Journal of Algebraic Combinatorics, which carried out the entire process within a period of months. The original title still exists but is rapidly declining into irrelevance, while the renamed Algebraic Combinatorics thrives. A larger list of journals that have declared independence is available.

* Back issues: this is not a problem for any library subscribing to the usual “Big Deal” package. In some cases, if libraries eventually cut these packages, they may lose access (although many deals do allow access to be retained after cancellation). Currently for example, Elsevier provides free access to back issues of mathematics journals from 4 years after publication date. If a journal ceases publishing, which is a likely outcome if the switch is done right, its back content is supposed to be available via services such as LOCKSS, CLOCKSS and Portico.

* No-compete contracts: the easiest solution is for an interim EiC to be named for a short time until the EiCs can resume their position. This is exactly what was done with Journal of Algebraic Combinatorics.

* Reputation: we acknowledge that some authors are under pressure to publish in journals with high impact factors, and that they may thus not want to submit to switching journals during the few years needed to get an impact factor; but our survey data shows that the community overwhelmingly care about editorial board research quality, peer review quality and ethical standards, and are for the most part very little concerned with impact factor or who publishes the journal: we thus expect sufficient good submissions for the journal to thrive through this IF-less window. The new incarnations of the mathematics journals mentioned above have a very high reputation: indeed, the reputation and citation impact usually increase because there is no loss in quality of refereeing, editing and  papers, while there is an improvement in ethics and often in efficiency. All these reputational issues are most easily dealt with by making a decisive switch backed by the research community, so that the true situation is made publicly very clear. 

* Zombie: Some people have expressed concerns in the other direction: the old journal may lose reputation and researchers who published with it before the switch may be unjustly penalized. This seems unlikely: it takes at least several years for reputations to change outside the community of users of a journal.

* Indexing is relatively straightforward. The most difficult is getting listed in databases such as Scopus and Thomson Reuters. However for an existing journal that is merely changing publisher (albeit with a possible name change) this is easier than for a completely new journal. Even in the latter case, as long as the journal is publishing regularly it is usually just a matter of filling in a form and waiting several months. We have experience with this and can assist if the lower-cost options are chosen, while the full service option we recommend will take care of all such matters.

* Stability and workload: these could be a problem if the conversion is done without sufficient resources. We are recommending converting the journal with financial backing. For example, the board of Journal of Algebraic Combinatorics moved the journal with support from organizations including Foundation Compositio Mathematica, RNBM (network of French mathematics libraries).
 
* On editorial payments: our survey shows that 43% of respondents are opposed to these on principle under any circumstances. Of course, administrative support is a different matter, and our recommended options allow for customized support at different funding levels. 

* Editorial software: to our knowledge the software used by providers such as Scholastica and Mersenne is at least as good (very likely considerably better) than that used by large commercial publishers. In addition, these providers are much more responsive to requests from editors.

Further questions and answers
======

Q. Are you suggesting switching to an open access journal with publication charges?

A. No. We are recommending that the journal become fully open access with no charge to authors.

Q. Then how will the journal be financially viable?

A journal such as Algebraic Combinatorics can be run for the foreseeable future on donated funds that we already have. We are in intensive discussions with national funders and library consortia to support an analogue of the Open Library of Humanities.  

Q. Discrete Analysis is an arXiv overlay journal, so it does not actually publish papers. Is that what you are suggesting for my journal?

A. Not necessarily: this is only one of several options. It would make no difference to the costs of Discrete Analysis if it hosted the papers on the journal's website. The main area in which costs are cut is in copy-editing, paywall maintenance, legal costs, etc. Like many newer journals, DA provides a style file but otherwise takes the attitude that the final author submission after refereeing (“postprint”) is usually perfectly adequate, as the widespread use of arXiv shows. Of course, paid copyediting can be included in any journal’s offering as funding permits.

Switching publisher does, of course, give an editorial board a good reason to review its procedures and consider innovations that may improve the journal’s quality.

Q. What about the K-Theory debacle?

A. It is important to avoid duplicating the sad case of the journal K-Theory, which broke away from a commercial publisher only to be involved in a battle between editors over ownership, (it  was owned personally by the editor-in-chief, supposedly temporarily). This is part of the reason that we suggest each journal be owned by a nonprofit organization. After its false start, K-Theory became Annals of K-Theory, which is owned by a non-profit foundation (http://www.ktheoryfoundation.org/). Other options include ownership by a learned society. The key point is to ensure that publishers provide services to the journal and do not constrain efforts by the editors, reviewers and authors to improve it. Publishers should compete on quality and price to provide these services and always face the possibility of losing the contract to another provider. Disputes among editors should be solved systematically by a specified transparent process, to be clearly stated by the nonprofit organization owning the journal. We have some experience with these issues and are happy to help.  Arguably the major damage caused by the K-Theory conflict has been the contested copyright status of the published papers (resulting in the inaccessibility of the archive); but this cannot arise when the papers are openly licensed (as suggested by the Fair OA principles).

Q. Will switching my journal make any difference to the current publishing system?

A. In one sense, no: companies such as Elsevier and Springer will continue to sell their big bundles of journals at high prices. However, for a major mathematics journal (or better still journals -- we are approaching editorial boards of many journals) to leave a major publisher would be a powerful signal that at least some people in the academic community have had enough. It would also weaken the negotiating position of the publishers, by providing an easy-to-follow example for other journals. It would be unlikely for any single journal switching to bring about a phase transition of the system, but it could play an important part in accelerating the change that is so badly needed, leading to a broader transition within a few years instead of several decades.

Q. How can anyone realistically provide professional journal services so much more cheaply than the charges of the major publishers suggest?

A. The major publishers have a large investment in old-fashioned systems which are much less efficient than those used by newer publishers. On top of that, many have large profit margins. Also, they provide and charge for some services that are of little or no value (such as managing payments from readers or authors, preventing non-authorized people from reading the articles, and arguably typesetting). Realistic estimates of the cost of a community-run open access journal range from near zero up to $500 per article, less than 15% of the price offered by the traditional publishers.

Q. Why haven’t more editorial boards already switched in the way you propose?

A. This is a good question. We believe that one reason is that there has never been a coordinated and determined attempt to facilitate such switching, which is precisely the point of the current project. Also, it is a classic collective action problem -- it is much easier to adopt an innovative new model if many others are already doing it. The goal of our current initiative is to normalize this behaviour among our colleagues. Another obstacle may have been perceived lack of alternative publishing options, although good independent publishers using the subscription model, such as MSP of Berkeley, have been operating for a long time. Until recently it was not easy to run an open access journal because of possible lack of income stream, but this is no longer a serious obstacle, hence this proposal.

I am still not quite convinced --- what now?
======

* MathOA board members  Benoit Kloeckner, Victor Reiner and Mark Wilson are available to discuss, as are MathOA advisory board members and the editors of Algebraic Combinatorics. The entire purpose of MathOA is to facilitate conversion of journals to a Fair Open Access model. We have done this from start to finish with Journal of Algebraic Combinatorics, and are continually raising funding for new journals.
