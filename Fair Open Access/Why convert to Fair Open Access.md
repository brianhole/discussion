Why convert my journal to Fair Open Access?
======

* You will help the research community to reclaim control of the journal. It will be easier to make innovations and improvements in journal processes.
* Publishers and other service providers interested in being involved will have to compete to satisfy the users of the journal. This will increase efficiency and quality. For example, the editorial software will probably be a major improvement on what you use now. 
* All papers published in your journal will immediately be freely available to anyone with an Internet connection: no paywalls, no embargo periods, no discrimination against readers in poorer countries. In particular, the public, who in most case subsidize the research, will be able to [access the results] (https://whoneedsaccess.org/).
* The impact of your journals will likely increase:  open access journal articles [attract higher citation rates] (https://sparceurope.org/what-we-do/open-access/sparc-europe-open-access-resources/open-access-citation-advantage-service-oaca/) and the publicity attending the journal conversion will help the journal's profile. 
* Mathematics [journals that declared independence](http://oad.simmons.edu/oadwiki/Journal_declarations_of_independence) from large commercial publishers [do better than their original versions](https://mcw.blogs.auckland.ac.nz/2016/10/08/what-happens-to-journals-that-break-away/).
* Libraries will move closer to being able to cancel exorbitant subscriptions, freeing up money for the research community to use more productively than donating to shareholders of high-profit commercial organizations.



